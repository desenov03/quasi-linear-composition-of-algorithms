import seaborn as sns
from matplotlib import pyplot as plt
from sklearn.ensemble import GradientBoostingRegressor, RandomForestRegressor
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import StackingRegressor
from sklearn.preprocessing import FunctionTransformer
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
import pandas as pd
from sklearn.preprocessing import LabelEncoder

data = pd.read_csv('weather_and_music.csv')

music_parametres = ['Energy','Danceability','Tempo','Speechiness']

music_parametres_result = []

for i in range(4):
    label_encoder = LabelEncoder()
    data['Weather Description'] = label_encoder.fit_transform(data['Weather Description'])

    data['Weather Description'] = pd.to_numeric(data['Weather Description'], errors='coerce')

    X_train, X_test, y_train, y_test = train_test_split(data[['Temperature', 'Weather Description']],
                                                        data[[music_parametres[i]]],
                                                        test_size=0.2, random_state=42)
    def select_weather_data(X):
        return X[['Temperature', 'Weather Description']]

    base_models = [
        ('linear', make_pipeline(FunctionTransformer(select_weather_data, validate=False), LinearRegression())),
        ('gradient_boosting', GradientBoostingRegressor(random_state=42))
    ]

    stacking_model = StackingRegressor(
        estimators=base_models,
        final_estimator=RandomForestRegressor(random_state=42),
        cv=5
    )

    stacking_model.fit(X_train, y_train)

    stacking_predictions = stacking_model.predict(X_test)

    mse_stacking = mean_squared_error(y_test, stacking_predictions)
    music_parametres_result.append(mse_stacking)

for i in range(4):
    print(f'MSE for {music_parametres[i]} prediction using Stacking: {music_parametres_result[i]}')

correlation = data.corr()
plt.figure(figsize=(10, 8))
sns.heatmap(correlation, annot=True, cmap='coolwarm', fmt='.2f', linewidths=0.5)
plt.title('Correlation Matrix')
plt.show()
